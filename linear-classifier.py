import sys

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt


def create_samples():
    """
    Generating two classes of random points in a 2D plane**
    :return:
    :rtype:
    """
    np.random.seed(1)
    negative = np.random.multivariate_normal(
        mean=[1, 2],
        cov=[[1, 0.5], [0.5, 1]],
        size=num_samples_per_class)
    positive = np.random.multivariate_normal(
        mean=[2, 1],
        cov=[[1, 0.5], [0.5, 1]],
        size=num_samples_per_class)
    return negative, positive


def create_weights():
    """
    Creating the linear classifier variables
    """
    input_dim = 2
    output_dim = 1
    _W = tf.Variable(
        initial_value=tf.random.uniform(shape=(input_dim, output_dim)))
    _b = tf.Variable(initial_value=tf.zeros(shape=(output_dim,)))
    return _W, _b


def fit(inputs, targets, learning_rate, W, b):
    """
    The batch training loop
    """
    for step in range(STEPS):
        print(f"Starting step {step}")
        print(f"W: {W.numpy()}")
        print(f"b: {b.numpy()}")
        loss = training_step(inputs, targets, learning_rate, W, b)
        print(f"Loss at step {step}: {loss:.4f}")


def training_step(inputs, targets, learning_rate, W, b):
    """
    The training step function
    """
    with tf.GradientTape() as tape:
        _predictions = model(inputs)
        loss = square_loss(_predictions, targets)
    grad_loss_wrt_W, grad_loss_wrt_b = tape.gradient(loss, [W, b])

    plot_classifier(W, b)
    print(f"predictions: {_predictions.numpy()}")
    plot_data(inputs, _predictions[:, 0] > 0.5)

    W.assign_sub(grad_loss_wrt_W * learning_rate)
    b.assign_sub(grad_loss_wrt_b * learning_rate)

    return loss


def model(inputs):
    """
    The forward pass function
    """
    return tf.matmul(inputs, W) + b

def square_loss(predictions, targets):
    """
    The mean squared error loss function
    """
    per_sample_losses = tf.square(predictions - targets)
    return tf.reduce_mean(per_sample_losses)


def plot_data(_inputs, data):
    """
    Plotting the two point classes
    """
    plt.scatter(_inputs[:, 0], _inputs[:, 1], c=data)
    plt.show()

def plot_classifier(W, b):
    x = np.linspace(-1, 4, 100)
    y = - W[0] / W[1] * x + (0.5 - b) / W[1]
    plt.plot(x, y, "-r")


if __name__ == '__main__':


    try:
        STEPS = int(sys.argv[1])
    except IndexError:
        STEPS = 5
        print(f'using default number of steps: {STEPS} (use different number as first argument when running this script)')


    learning_rate = 0.1
    num_samples_per_class = 1000

    negative_samples, positive_samples = create_samples()

    # Stacking the two classes into an array with shape (2000, 2)
    inputs = np.vstack((negative_samples, positive_samples)).astype(np.float32)

    # Generating the corresponding targets (0 and 1)
    targets = np.vstack((np.zeros((num_samples_per_class, 1), dtype="float32"),
                         np.ones((num_samples_per_class, 1), dtype="float32")))


    W, b = create_weights()

    fit(inputs, targets, learning_rate, W, b)
